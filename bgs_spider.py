import sys
import scrapy
from scrapy.spiders import CrawlSpider, Rule
from scrapy.linkextractors import LinkExtractor
from scrapy.selector import Selector
from scrapy.utils.url import urljoin_rfc
from urllib.request import urlopen
from urllib.parse import urlparse
import http.client
import re
import time
from scrapy.spidermiddlewares.httperror import HttpError
from twisted.internet.error import DNSLookupError
from twisted.internet.error import TimeoutError, TCPTimedOutError
from scrapy.item import Item, Field
import logging

logger = logging.getLogger("bgs."+__name__)
logger.setLevel(logging.INFO)

class BGSItem(Item):
    # define the fields for your item here like:
    id = Field()
    rock_cat_raw = Field()
    rock_cat = Field()
    comment = Field()
    map_ref = Field()
    map_coord = Field()
    image_xpl = Field() #Image path for cross polarisation
    image_ppl = Field() #Image path for plane polarisation
    no_images = Field()
    path_disk = Field()


class BGSSpider(scrapy.Spider):
    name = "bgs_spider"

    def __init__(self, *args, **kwargs):
        super(BGSSpider, self).__init__(*args, **kwargs)
        try:
            endpoint = kwargs.get('start_url')
            self.start_urls = [endpoint]

        except Exception as e:
            logger.debug('In bgs spider init:' + str(e))
        #settings.overrides['DEPTH_LIMIT'] = self.max_depth
        self.visited_id = set()
        self.images_path = kwargs.get("images_path")
        logger.info("Images path: {}".format(self.images_path))
        logger.debug("Settings:\n* Start_urls = " + ', '.join(self.start_urls))

    def set_result(self, results):
        self.results = results

    def get_result(self):
        return self.results

    def parse(self, response):

        is_list = response.meta.get("is_list", True)

        if is_list:
            logger.info("New page list at {}".format(response.url))
            base_url = response.url[8:].split("/")[0]
            base_url = "https://" + base_url
            thin_petrography_list = response.xpath('//div[@class="sectionBody"]/table//tr/td/a/@href').getall()

            #print(len(thin_petrography_list))
            for thin_petrography_url in thin_petrography_list:
                #logger.info(base_url + thin_petrography_url)
                req = scrapy.Request(base_url + thin_petrography_url,
                                         method="GET", callback=self.parse,
                                         errback=self.errback_httpbin, dont_filter=True,
                                         meta={"is_list" : False})
                yield req

            next_page = response.xpath('//ul[@class="paging"]/li[@class="next"]/a/@href').get()


            if not next_page is None:
                req = scrapy.Request(base_url + next_page,
                                         method="GET", callback=self.parse,
                                         errback=self.errback_httpbin, dont_filter=True,
                                         meta={"is_list" : True})
                #logger.info("trying req for next page with {}".format(base_url + next_page))
                yield req
            else:
                logger.info("parsing finished for url {}".format(response.url))
        else:

            thin_item = BGSItem()

            thin_item["image_xpl"] = response.xpath('//div[@class="sectionBody"]/div[@class="rightCol70 border"]/div[@class="sliderImg"]/@style').get()

            id = [x for x in response.url.split("&") if x.startswith("sampleId")][0].split("=")[-1]

            #logger.info(thin_item["image_xpl"])
            if id in self.visited_id or thin_item["image_xpl"] is None:
                thin_item["no_images"] = True
                yield thin_item
            else:
                self.visited_id.add(id)

                thin_item["no_images"] = False
                m = re.search('url\((.+)\); filter', thin_item["image_xpl"])

                if m:
                    thin_item["image_xpl"] = "https://" + m.group(1)[2:]
                else:
                    thin_item["no_images"] = True
                    logger.warning("cannot find url in {}".format(thin_item["image_xpl"]))
                    yield thin_item

                thin_item["image_ppl"] = "https://" + response.xpath('//div[@class="sectionBody"]/div[@class="rightCol70 border"]/div[@class="sliderImg"]//img/@src').get()[2:]

                thin_item["id"] = id

            #logger.info(thin_item["id"])

                thin_item["rock_cat_raw"] = response.xpath('//div[@class="sectionBody"]/table//tr/td/text()').getall()[2]
            #logger.info(thin_item["rock_cat_raw"])
                thin_item["rock_cat"] = thin_item["rock_cat_raw"].strip(" '\"").split()
                thin_item["comment"] = response.xpath('//div[@class="sectionBody"]/table//tr/td/text()').getall()[4]
                thin_item["map_ref"] = response.xpath('//div[@class="sectionBody"]/table//tr/td/text()').getall()[9]
            #logger.info(thin_item["map_ref"])
                thin_item["map_coord"] = response.xpath('//div[@class="sectionBody"]/table//tr/td/text()').getall()[6]
                thin_item["path_disk"] = self.images_path

            yield thin_item

    def errback_httpbin(self, failure):
        # log all failures
        self.logger.error(repr(failure))

        # in case you want to do something special for some errors,
        # you may need the failure's type:

        if failure.check(HttpError):
            # these exceptions come from HttpError spider middleware
            # you can get the non-200 response
            response = failure.value.response
            self.logger.error('HttpError on %s', response.url)

        elif failure.check(DNSLookupError):
            # this is the original request
            request = failure.request
            self.logger.error('DNSLookupError on %s', request.url)

        elif failure.check(TimeoutError, TCPTimedOutError):
            request = failure.request
            self.logger.error('TimeoutError on %s', request.url)
