import numpy as np
import pandas as pd
import efficientnet_custom as efn
from keras.preprocessing.image import ImageDataGenerator
from ast import literal_eval
from sklearn.model_selection import train_test_split
from sklearn.model_selection import StratifiedShuffleSplit
from keras.utils import to_categorical
from keras import layers
from keras.models import Model
import keras
try:
    from keras.callbacks.callbacks import ModelCheckpoint, EarlyStopping, ReduceLROnPlateau
except:
    from keras.callbacks import ModelCheckpoint, EarlyStopping, ReduceLROnPlateau
from skmultilearn.model_selection import IterativeStratification
import argparse
import cv2
import pickle

g_mean_std = np.load("mean_std.npz")

g_cat_pickle_name = "categories.pickle"

import tensorflow as tf
K = keras.backend

class MetricsAtTopK:
    def __init__(self, k):
        self.k = k

    def _get_prediction_tensor(self, y_pred):
        """Takes y_pred and creates a tensor of same shape with 1 in indices where, the values are in top_k
        """
        topk_values, topk_indices = tf.nn.top_k(y_pred, k=self.k, sorted=False, name="topk")
        # the topk_indices are along last axis (1). Add indices for axis=0
        ii, _ = tf.meshgrid(tf.range(tf.shape(y_pred)[0]), tf.range(self.k), indexing='ij')
        index_tensor = tf.reshape(tf.stack([ii, topk_indices], axis=-1), shape=(-1, 2))
        prediction_tensor = tf.sparse_to_dense(sparse_indices=index_tensor,
                                               output_shape=tf.shape(y_pred),
                                               default_value=0,
                                               sparse_values=1.0,
                                               validate_indices=False
                                               )
        prediction_tensor = tf.cast(prediction_tensor, K.floatx())
        return prediction_tensor

    def true_positives_at_k(self, y_true, y_pred):
        prediction_tensor = self._get_prediction_tensor(y_pred=y_pred)
        true_positive = K.sum(tf.multiply(prediction_tensor, y_true))
        return true_positive

    def false_positives_at_k(self, y_true, y_pred):
        prediction_tensor = self._get_prediction_tensor(y_pred=y_pred)
        true_positive = K.sum(tf.multiply(prediction_tensor, y_true))
        c2 = K.sum(prediction_tensor)  # TP + FP
        false_positive = c2 - true_positive
        return false_positive

    def false_negatives_at_k(self, y_true, y_pred):
        prediction_tensor = self._get_prediction_tensor(y_pred=y_pred)
        true_positive = K.sum(tf.multiply(prediction_tensor, y_true))
        c3 = K.sum(y_true)  # TP + FN
        false_negative = c3 - true_positive
        return false_negative

    def precision_at_k(self, y_true, y_pred):
        prediction_tensor = self._get_prediction_tensor(y_pred=y_pred)
        true_positive = K.sum(tf.multiply(prediction_tensor, y_true))
        c2 = K.sum(prediction_tensor)  # TP + FP
        return true_positive/(c2+K.epsilon())

    def recall_at_k(self, y_true, y_pred):
        prediction_tensor = self._get_prediction_tensor(y_pred=y_pred)
        true_positive = K.sum(tf.multiply(prediction_tensor, y_true))
        c3 = K.sum(y_true)  # TP + FN
        return true_positive/(c3+K.epsilon())

    def f1_at_k(self, y_true, y_pred):
        precision = self.precision_at_k(y_true=y_true, y_pred=y_pred)
        recall = self.recall_at_k(y_true=y_true, y_pred=y_pred)
        f1 = (2*precision*recall)/(precision+recall+K.epsilon())
        return f1

from keras import backend as K
from sklearn.utils.class_weight import compute_class_weight

def categorical_focal_loss(alpha, gamma=2.):
    """
    Softmax version of focal loss.
           m
      FL = ∑  -alpha * (1 - p_o,c)^gamma * y_o,c * log(p_o,c)
          c=1
      where m = number of classes, c = class and o = observation
    Parameters:
      alpha -- the same as weighing factor in balanced cross entropy
      gamma -- focusing parameter for modulating factor (1-p)
    Default value:
      gamma -- 2.0 as mentioned in the paper
      alpha -- 0.25 as mentioned in the paper
    References:
        Official paper: https://arxiv.org/pdf/1708.02002.pdf
        https://www.tensorflow.org/api_docs/python/tf/keras/backend/categorical_crossentropy
    Usage:
     model.compile(loss=[categorical_focal_loss(alpha=.25, gamma=2)], metrics=["accuracy"], optimizer=adam)
    """
    alpha_w = K.variable(value=alpha, dtype="float32")

    def categorical_focal_loss_fixed(y_true, y_pred):
        """
        :param y_true: A tensor of the same shape as `y_pred`
        :param y_pred: A tensor resulting from a softmax
        :return: Output tensor.
        """

        # Scale predictions so that the class probas of each sample sum to 1
        #y_pred /= K.sum(y_pred, axis=-1, keepdims=True)

        # Clip the prediction value to prevent NaN's and Inf's
        epsilon = K.epsilon()
        y_pred = K.clip(y_pred, epsilon, 1. - epsilon)

        # Calculate Cross Entropy
        cross_entropy = -y_true * K.log(y_pred)

        # Calculate Focal Loss
        loss = alpha_w * K.pow(1 - y_pred, gamma) * cross_entropy

        # Average the losses in mini_batch
        return K.mean(loss, axis=1)

    return categorical_focal_loss_fixed


import keras.backend.tensorflow_backend as tfb

def create_w_binary_crossentropy(pos_weight=10):
    def weighted_binary_crossentropy(target, output):
        """
        Weighted binary crossentropy between an output tensor
        and a target tensor. POS_WEIGHT is used as a multiplier
        for the positive targets.

        Combination of the following functions:
        * keras.losses.binary_crossentropy
        * keras.backend.tensorflow_backend.binary_crossentropy
        * tf.nn.weighted_cross_entropy_with_logits
        """
        # transform back to logits
        _epsilon = tfb._to_tensor(tfb.epsilon(), output.dtype.base_dtype)
        output = tf.clip_by_value(output, _epsilon, 1 - _epsilon)
        output = tf.log(output / (1 - output))
        # compute weighted loss
        loss = tf.nn.weighted_cross_entropy_with_logits(labels=target,
                                                    logits=output,
                                                    pos_weight=pos_weight)
        return tf.reduce_mean(loss, axis=-1)

    return weighted_binary_crossentropy

def iterative_train_test_split(X, y, test_size, random_state=None):

    stratifier = IterativeStratification(n_splits=2, order=2,
        sample_distribution_per_fold=[test_size, 1.0-test_size], random_state=random_state)

    train_indexes, test_indexes = next(stratifier.split(X, y))

    return train_indexes, test_indexes


def preprocess_input(x):
    x /= 255.

    x[..., 0] -= g_mean_std["mean_xpl"][0]
    x[..., 1] -= g_mean_std["mean_xpl"][1]
    x[..., 2] -= g_mean_std["mean_xpl"][2]
    x[..., 0] /= g_mean_std["std_xpl"][0]
    x[..., 1] /= g_mean_std["std_xpl"][1]
    x[..., 2] /= g_mean_std["std_xpl"][2]

    x[..., 3] -= g_mean_std["mean_ppl"][0]
    x[..., 4] -= g_mean_std["mean_ppl"][1]
    x[..., 5] -= g_mean_std["mean_ppl"][2]
    x[..., 3] /= g_mean_std["std_ppl"][0]
    x[..., 4] /= g_mean_std["std_ppl"][1]
    x[..., 5] /= g_mean_std["std_ppl"][2]

    return x

def count_cat_remove(df, categories):
  count_cat = {}
  for c in categories:
        count_cat[c] = 0

  for row in df.itertuples():
      for c in row.rock_category_clean:
          count_cat[c] += 1

  to_remove = []
  for c, v in count_cat.items():
    if v < 10:
      to_remove.append(c)

  print(to_remove)
  print(len(df))
  for c in to_remove:
    df = df[df["rock_category_clean"] != {c}]

  print(len(df))
  return df, list(set(categories) - set(to_remove))

def prepare_dataset(df, create_categories=False):

    df.loc[:, "rock_category_clean"] = df[["rock_category_clean"]].apply(lambda x: literal_eval(x.values[0]), axis=1)
    df.loc[:, "cat_count"] = df[["rock_category_clean"]].apply(lambda x: len(list(x.values[0])), axis=1)

    df = df[df["cat_count"] == 1]

    if create_categories:
        categories = set()
        for c in df["rock_category_clean"].values:
            categories |= c

        categories = list(categories)
        
        df, categories = count_cat_remove(df, categories)
        with open(g_cat_pickle_name, 'wb') as f:
            pickle.dump(categories, f)
    else:
        with open(g_cat_pickle_name, 'rb') as f:
            categories = pickle.load(f)

        return df, categories

    num_classes = len(categories)

    df.loc[:, "rock_category_list"] = df[["rock_category_clean"]].apply(lambda x : [categories.index(c) for c in x.values[0]], axis=1)

    def transform_categorical(x):
        x = x.values[0]
        res = np.zeros(num_classes, dtype=np.float32)
        for c in x:
            res = to_categorical(c, num_classes=num_classes)
            break

        return res

    df.loc[:, "labels"] = df[["rock_category_list"]].apply(transform_categorical, axis=1)

    return df, categories

def split_dataset(df, categories, test_size=0.15):

    df.loc[:, "labels_sparse"] = df[["rock_category_list"]].apply(lambda x : x.values[0][0], axis=1)

    print(df)
    df_train, df_val = train_test_split(df, test_size=test_size,
                                    random_state=42, stratify=df[["labels_sparse"]])

    print(len(df_train))
    print(len(df_val))

    return df_train, df_val

def prepare_datagen(datagen, df, im_size, class_weights=None,
                    batch_size=32, validation=False):

    def sanitize_labels(x):
        x = x.values[0]
        if isinstance(x, str):
            #print(x)
            x = x.strip("[").strip("]").split()
            x = [float(s) for s in x]
            #print(x)
        elif not isinstance(x, list):
            x = list(x)

        return x
    df.loc[:, "labels"] = df[["labels"]].apply(sanitize_labels, axis=1)

    #print(df["labels"])
    #gen = datagen.flow_from_dataframe(df, directory='.',
    #        x_col='image_xpl', y_col='labels',
    #        target_size=(380, 380), batch_size=batch_size)

    def generator(df, batch_size, im_size, class_weights=None):
        indices = np.arange(len(df))
        batch_x = []
        batch_y = []
        batch_w = []

        while True:
            np.random.shuffle(indices)

            for idx in indices:

                img_xpl = cv2.imread(df.iloc[idx].at["image_xpl"])
                img_ppl = cv2.imread(df.iloc[idx].at["image_ppl"])

                img_xpl = cv2.resize(img_xpl, (im_size, im_size), interpolation=cv2.INTER_AREA)
                img_ppl = cv2.resize(img_ppl, (im_size, im_size), interpolation=cv2.INTER_AREA)

                if not validation:
                    trans = datagen.get_random_transform(img_xpl.shape)
                    img_xpl = datagen.apply_transform(img_xpl, trans)
                    img_ppl = datagen.apply_transform(img_ppl, trans)

                img = np.concatenate([img_xpl, img_ppl], axis=-1).astype(np.float32)

                batch_x.append(img)
                batch_y.append(df.iloc[idx].at["labels"])
                if not class_weights is None:
                    w = 1
                    for idx, y in enumerate(batch_y[-1]):
                        if y == 1:
                            w *= class_weights[idx]

                    batch_w.append(w)

                if len(batch_x) >= batch_size:

                    batch_x = preprocess_input(np.array(batch_x))
                    batch_y = np.array(batch_y)

                    if not class_weights is None:
                        batch_w = np.array(batch_w)

                        yield (batch_x, batch_y, batch_w)
                        batch_w = []
                    else:
                        yield (batch_x, batch_y)

                    batch_x = []
                    batch_y = []


    return generator(df, batch_size, im_size, class_weights)

def get_model(inp_shape, num_classes, weights=None):

    model = efn.EfficientNetB1(weights="imagenet", input_shape=inp_shape,
                                include_top=False)

    x = layers.GlobalAveragePooling2D(name='avg_pool')(model.output)
    x = layers.Dense(num_classes,
                         activation='softmax',
                         kernel_initializer={
                                        'class_name': 'VarianceScaling',
                                        'config': {
                                                'scale': 1. / 3.,
                                                'mode': 'fan_out',
                                                'distribution': 'uniform'
                         }},
                        name='probs')(x)

    complete_model = Model(inputs=model.input, outputs=x, name="ENetB1_rock")

    if not weights is None:
        complete_model.load_weights(weights)

    return complete_model

def get_callbacks(weight_name):

    callbacks = []

    callbacks.append(ReduceLROnPlateau(monitor='val_loss', factor=0.5, patience=3
                                        , verbose=1,
                                        min_lr=5e-7))

    callbacks.append(EarlyStopping(monitor='val_loss', patience=7))

    weight_name = weight_name[:-5] + ".{epoch:02d}-{val_loss:.2f}.hdf5"
    callbacks.append(ModelCheckpoint(weight_name, monitor='val_loss',
                    save_weights_only=True))

    return callbacks

def create_categories_weight(df, categories):

    y = [c for c in df["labels_sparse"].values]
    return np.array(compute_class_weight("balanced", np.unique(y), y)) + 1

import sys
if __name__ == "__main__":

    parser = argparse.ArgumentParser(description="Running and testing")
    parser.add_argument('--dataset', required=True, type=str, help="dataset file")
    parser.add_argument('--split', action='store_true', default=False, help="Create a split for dataset")
    parser.add_argument('--train', action='store_true', help="Train the model")
    parser.add_argument('--num_epoch', type=int, default=100, help="Number of epoch for training")
    parser.add_argument('--batch_size', type=int, default=32, help="Batch size for inference or training")
    parser.add_argument('--im_size', type=int, default=380, help="Input image size rescaling")
    parser.add_argument('--lr', type=float, default=1e-3, help="Initial learning rate")
    parser.add_argument('--weights', type=str, help="weight file for the model")
    parser.add_argument('--weights_save', default="rock_classification.hdf5",
                        type=str, help="weight file for saving the model")
    parser.add_argument('--test', action='store_true', default=False, help="Test the model")
    args = parser.parse_args()

    base_dataset_name = args.dataset[:-4]
    df = pd.read_csv(args.dataset)
    df, categories = prepare_dataset(df, args.split)

    print(categories)
    num_classes = len(categories)

    if args.split and (args.train or args.test):
        print('Warning : you attemtped to use split and train or test. Split will recreate the val and train dataset, thus preventing continuation of previous training')
        sys.exit(1)
    elif args.split:
        df_train, df_val = split_dataset(df, categories)
        df_train.to_csv(base_dataset_name + "_train.csv", index=False)
        df_val.to_csv(base_dataset_name + "_val.csv", index=False)
    else:
        df_train = pd.read_csv(base_dataset_name + "_train.csv")
        df_val = pd.read_csv(base_dataset_name + "_val.csv")


        datagen_args = {"horizontal_flip":True, "vertical_flip":True, "zoom_range":0.15,
                    "rotation_range":20, "fill_mode" : "constant",
                    "shear_range": 10, "width_shift_range": 0.1, "height_shift_range": 0.1}
        print(datagen_args)
        datagen = ImageDataGenerator(**datagen_args)

        weights_cat = create_categories_weight(df_train, categories)
        print(list(weights_cat))


        inp_shape = (args.im_size, args.im_size, 6)
        model = get_model(inp_shape, num_classes, args.weights)

        weights_cat_dict = {}
        for idx, w in enumerate(weights_cat):
            weights_cat_dict[idx] = w

        train_gen = prepare_datagen(datagen, df_train, args.im_size, class_weights=None, batch_size=args.batch_size)
        val_gen = prepare_datagen(datagen, df_val, args.im_size, class_weights=None, batch_size=args.batch_size, validation=True)
        if args.train:

            optimizer = keras.optimizers.Adam(lr=args.lr)
            model.compile(optimizer=optimizer, loss=categorical_focal_loss(alpha=np.ones_like(weights_cat), gamma=2.),
                        metrics=["accuracy"])

            history = model.fit_generator(train_gen, steps_per_epoch=(len(df_train) // args.batch_size + 1),
                            epochs=args.num_epoch,
                            callbacks=get_callbacks(args.weights_save),
                            validation_data=val_gen,
                            validation_steps=(len(df_val) // args.batch_size + 1),
                            verbose=1, class_weight=weights_cat_dict)
            with open("history.pickle", 'wb') as f:
                pickle.dump(history, f)
        elif args.test:
            train_batch_x, train_batch_y, _ = next(train_gen)
            val_batch_x, val_batch_y, _ = next(train_val)

