# Installation

### prérequis

- Installer python3

- Installer les modules pythons à l'aide de la commande
 `pip3 install --user -r requirements.txt`

- Créer un dossier "images" vide s'il n'existe pas déjà

# Description

Le projet se compose d'une partie scraper qui va construire le dataset 
à partir du site [https://www.bgs.ac.uk/](https://www.bgs.ac.uk/)
ainsi que d'une partie servant à assainir le dataset, le préparer et entrainer un réseau de neurone avec CNN pour
classifier les lames minces.

## Scraper

Cette partie se compose des fichiers suivants:
 - bgs_spider.py
 - build_dataset_pipeline.py
 - scrapy_runner.py

 ### Usage

Il suffit de lancer `python3 scrapy_runner.py` et le module va créer le dataset.

A noter:
 - le scraping peut etre très long, environ 10 h pour faire la moitié des données du site
 - le dataset peut vite prendre de la place, environ 40 Go pour 70 000 éléments
 - pour arrêter la collecte du dataset à tout moment et créer en sortie un fichier "dataset.csv" il suffit d'envoyer un signal d'arrêt au programme
 ou de faire CTR + C une seule fois et d'attendre que le programme s'arrête. Si l'on n'envoit le signal plusieurs fois le fichier "dataset.csv" ne sera pas créer
 - si l'on veut refaire la collecte du dataset ou reprendre là ou la recherche s'est arrêté précedemment il n'y a pas besoin de retélécharger les images déjà présentes et le scraping est très rapide sur la partie déjà vu.


## Entrainement

Cette partie se compose des fichiers suivants:
- sanitize_dataset.py
- compute_mean_std.py
- run.py

### Usage

- A partir d'un fichier "dataset.csv" on doit d'abord le nettoyer:
  - `python3 sanitize_dataset.py --data <fichier de dataset> --out <fichier de sortie>`
  - On obtient ainsi un ficher "dataset_clean.csv"


- Ensuite il faut calculer la moyenne et déviation standards du dataset:
  - `python3 compute_mean_std.py <fichier de dataset clean>`
  - Un fichier "mean_std.npz" sera créée à l'issu du script.


- Maintenant il convient de séparer le dataset en partie entrainement et validation:
  - `python3 run.py --dataset <fichier de dataset clean> --split`
  - un fichier "categories.pickle" et deux fichiers "dataset_clean_train.csv" et "dataset_clean_val.csv" seront créés

- Enfin pour lancer l'entrainement:
  - `python3 run.py --dataset <fichier de dataset clean> --train --batch_size <Taille de batch> --num_epoch <Nombre d'epochs>`
