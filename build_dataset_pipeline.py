import scrapy
import logging
import pandas as pd
import requests
import shutil
import os

logger = logging.getLogger("bgs."+__name__)

def downloadImage(url, id, suffix, path_disk):

    path = "images"

    #Downloading images
    img_path = os.path.join(path, id + suffix + ".jpg")
    img_path_disk = os.path.join(path_disk, id + suffix + ".jpg")

    if os.path.isfile(img_path_disk):
        return img_path

    r = requests.get(url, stream=True)
    if r.status_code == 200:

        with open(img_path_disk, "wb") as f:
            r.raw.decode_content = True
            shutil.copyfileobj(r.raw, f)

        return img_path

    return None

class BuildDatasetPipeline(object):

    """! @brief A custom pipeline that stores dataset extracted and download images"""
    def open_spider(self, spider):
       self.dataset = None
       self.dataset_dict = {"id" : [], "image_xpl" : [], "image_ppl" : [],
                            "rock_category_raw" : [], "comment" : [], "map_ref" : [], "map_coord": []}
       self.num_item = 0

    def process_item(self, item, spider):
           if item["no_images"]:
                return

           self.num_item += 1
           print("Number item done : {}".format(self.num_item), end="\r")

           image_xpl = downloadImage(item["image_xpl"], item["id"], "_xpl", item["path_disk"])
           image_ppl = downloadImage(item["image_ppl"], item["id"], "_ppl", item["path_disk"])

           if image_xpl is None or image_ppl is None:
                return

           self.dataset_dict["id"].append(item["id"])
           self.dataset_dict["image_xpl"].append(image_xpl)
           self.dataset_dict["image_ppl"].append(image_ppl)
           self.dataset_dict["rock_category_raw"].append(item["rock_cat_raw"])
           self.dataset_dict["comment"].append(item["comment"])
           self.dataset_dict["map_ref"].append(item["map_ref"])
           self.dataset_dict["map_coord"].append(item["map_coord"])

    def close_spider(self, spider):

      self.dataset = pd.DataFrame.from_dict(self.dataset_dict)
      spider.set_result(self.dataset)


