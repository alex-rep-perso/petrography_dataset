import pandas as pd
import argparse
from functools import partial
import re

MINERALS_CATEGORY_LIST = [
'actinolite', 'aegirine', 'albite',
'anorthite', 'ankerite',
'anthophylite', 'biotite', 'calcite', 'chlorite',
'clay minerals', 'corundum', 'diopside', 'dolomite',
'enstatite', 'epidote', 'feldspar', 'feldspars', 'fluorite',
'graphite', 'haematite', 'halite', 'halloysite',
'hedenbergite', 'hornblende', 'hypersthene', 'illite', 'kaolin',
'leucite', 'limonite', 'magnesite', 'magnetite', 'mica', 'microcline',
'montmorillonite', 'muscovite', 'nepheline', 'olivine', 'opal',
'orthoclase', 'plagioclase feldspar', 'alkaline feldspar', 'pyrite', 'pyroxene',
'pyrrhotite', 'quartz', 'sericite', 'serpentine', 'talc',
'tremolite', 'varves', 'vermicullite', 'zeolite', 'riebeckite', 'schorl',
'phosphorite', 'phosphorites', 'siderite', 'plagioclase']

SYNONYM = [['sandstone', 'wacke', 'arenite', 'greywacke', 'grit'], ['lamprophyre', 'lamprophyres', 'camptonite', 'monchiquite'],
            ['shale', 'shales'], ['diorite', 'dioritic', 'microdiorite'], ['elvan', 'elvanite'],
           ['mudstone', 'pelite', 'mudrock', 'lutite', 'pelyte'], ['breccia', 'peperite'], ['gabbro', 'troctolite'],
           ['granite', 'aplogranite', 'elvan'], ['phosphorite', 'phosphorites'],
           ["grain", "grains"], ["dolerite", 'microgabbro'], ['calcsilicate', 'calc-silicate'],
           ["tuff", "lava"], ["metamudstone", "metapelite", "semipelite"], ["porphyry", "quartz-porphyry", "quartzporphyry", "porphyrite"]]

ROCK_CATEGORY_LIST = [
'limestone', 'agglomerate',
'schist', 'amphibolite', 'gneiss', 'andesite', 'anorthosite',
'arkosite', 'basalt', 'bauxite', 'shale', 'pelyte',
'breccia', 'chalk', 'chamockite', 'chert', 'clastic', 'claystone',
'conglomerate', 'dacite', 'dolerite', 'diorite', 'dolomite', 'dolostone',
'dunite', 'eclogite', 'essexite', 'evaporite', 'flagstone', 'gabbro',
'granite', 'granodiorite', 'granulite', 'greenschist', 'greenstone', 'greywacke',
'hornfels', 'hornblendite', 'siltstone', 'latite', 'syenodorite', 'lava', 'marble',
'migmatite',
'monzonite', 'mudstone', 'mylonite', 'molasse', 'nepheline syenite',
'norite', 'olivinite', 'oolite', 'paragneiss', 'pegmatite', 'peridotite',
'phonolite', 'phyllite', 'psammite', 'pumice','quartz', 'quartzite',
'porphyry', 'porphyrite', 'rhyodacite', 'rhyolite', 'sandstone', 'shale', 'shales',
'skarn', 'kersantite', 'keratophyre', 'dioritic', 'metamudstone', 'metapelite',
'slate', 'soapstone', 'sparagmite', 'syenite', 'tonalite', 'trachyte',
'tuff', 'tuffite', 'lamprophyre', 'lamprophyres', 'gravel', 'silt', 'metadolostone',
'ironstone', 'microgranite', 'phosphorite', 'phosphorites', 'wacke', 'siderite',
'elvan', 'elvanite', 'pelite', 'lutite', 'metadiorite', 'semipelite',
'microdiorite', 'grit', 'mugearite', 'spessartite', 'peperite', 'troctolite',
'aplogranite', 'grain', 'grains', 'camptonite', 'serpentinite', 'flint',
'monchiquite', 'arenite', 'microgabbro', 'calcsilicate', 'calc-silicate',
"quartz-porphyry", "quartzporphyry"]


if __name__ == "__main__":

    parser = argparse.ArgumentParser(description="Sanitize dataset")
    parser.add_argument('--data', required=True, type=str, help="dataset file")
    parser.add_argument('--out', default="dataset_clean.csv", type=str, help="out file")
    args = parser.parse_args()

    df = pd.read_csv(args.data)

    print(len(df))

    df_clean = df

    original_entry = re.compile("(.*)'Original entry: (.+)'(.*)")

    def search_original_entry(x):
        x = x.values[0]
        #print(x)
        m = original_entry.search(x)
        if m:
            return m.group(1) + ";" + m.group(2) + ";" + m.group(3)
        return x

    parenthesis = re.compile("\(.*\)")
    split_tiret = re.compile("-")
    splitter = re.compile("[0-9,;/.'?> +]['0-9,;/. ?>+]*[,;/.]?")
    split_with = re.compile("[Ww]ith")

    def extract_category_list(x, cat_list):
        x = x.values[0]
        rocks_list = [r for r in x if r in cat_list]

        return set(rocks_list)

    def separate_primary_secondary(x, primary=True):
        x = x.values[0]
        res = [s.strip().lower() for s in split_with.split(x) if len(s) > 0]

        if primary:
            return res[0]
        else:
            if len(res) > 1:
                return res[1]
            else:
                return ""

    def extract_split(x):
        x = x.values[0]
        x = parenthesis.sub("", x)
        res = [s.strip().lower() for s in splitter.split(x) if len(s) > 0]
        to_add = []
        for r in res:
            to_add += [s.strip().lower() for s in split_tiret.split(r) if len(s) > 0]

        return set(res + to_add)

    def reduce_synonym(x):
        x = x.values[0]
        res = set()

        for c in x:
            has_synonym = False
            for s in SYNONYM:
                if c in s:
                    has_synonym = True
                    res.add(s[0])
            if not has_synonym:
                res.add(c)

        if res >= {"quartz", "porphyry"} or res >= {"quartz", "dolerite"}:
            res = res - {"quartz"}

        return res

    df_clean.loc[:, "rock_category"] = df_clean[["rock_category_raw"]].apply(search_original_entry, axis=1)
    #print(df_clean["rock_category"])

    df_clean.loc[:, "rock_category_primary"] = df_clean[["rock_category"]].apply(separate_primary_secondary, axis=1)
    df_clean.loc[:, "rock_category_secondary"] = df_clean[["rock_category"]].apply(partial(separate_primary_secondary, primary=False), axis=1)

    print("separated primary")
    df_clean.loc[:, "rock_category_primary"] = df_clean[["rock_category_primary"]].apply(extract_split, axis=1)
    df_clean.loc[:, "rock_category_secondary"] = df_clean[["rock_category_secondary"]].apply(extract_split, axis=1)
    #print(df_clean["rock_category"])
    print("extract_split done")

    df_clean.loc[:, "rock_category_clean"] = df_clean[["rock_category_primary"]].apply(partial(extract_category_list, cat_list=ROCK_CATEGORY_LIST)
                                                                                , axis=1)

    df_clean.loc[:, "minerals_category_clean"] = df_clean[["rock_category_primary"]].apply(partial(extract_category_list, cat_list=MINERALS_CATEGORY_LIST)
                                                                                , axis=1)

    df_clean.loc[:, "comment_clean"] = df_clean[["comment"]].apply(extract_split, axis=1)
    df_clean.loc[:, "comment_clean"] = df_clean[["comment_clean"]].apply(partial(extract_category_list, cat_list=ROCK_CATEGORY_LIST)
                                                                                , axis=1)

    cond = df_clean["rock_category_clean"] != set()

    df_clean.loc[:, "rock_category_clean"] = df_clean.loc[:, "rock_category_clean"].where(cond, df_clean.loc[:, "comment_clean"])
    df_clean.loc[:, "rock_category_clean"] = df_clean[["rock_category_clean"]].apply(reduce_synonym, axis=1)
    print(df_clean[["rock_category_clean", "minerals_category_clean"]])

    categories = set()
    for c in df_clean["rock_category_clean"].values:
        categories |= c

    print(len(categories))
    print(categories)

    print(len(df_clean.loc[df_clean["rock_category_clean"] == set()]))

    df_clean = df_clean.loc[df_clean["rock_category_clean"] > set()]

    count_cat = {}

    for c in categories:
        count_cat[c] = 0

    for row in df_clean.itertuples():
        for c in row.rock_category_clean:
            count_cat[c] += 1


    for c, v in count_cat.items():
        if v < 10:
            print(c, v)
            df_clean = df_clean.loc[df_clean["rock_category_clean"] != {c}]


    print(len(df_clean))

    df_clean.to_csv(args.out, index=False)
