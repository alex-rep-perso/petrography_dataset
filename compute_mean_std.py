import cv2
import os
import numpy as np
import pandas as pd
import sys
import argparse

if __name__ == "__main__":

    parser = argparse.ArgumentParser()
    parser.add_argument("--dataset", required=True, type=str, help="dataset clean file")
    parser.add_argument("--images_folder", default="images", type=str, help="Folder to where images are")

    args = parser.parse_args()

    df = pd.read_csv(args.dataset)

    num_entries = len(df)
    # Everything is in BGR order
    mean_xpl = np.zeros(3)
    std_xpl = np.zeros(3)

    mean_ppl = np.zeros(3)
    std_ppl = np.zeros(3)

    print("Computing means for {} entries".format(num_entries))

    all_mean_xpl = np.zeros((num_entries, 3))
    all_mean_ppl = np.zeros((num_entries, 3))
    count = 0
    for row in df[["image_xpl", "image_ppl"]].itertuples():

        img_xpl = cv2.imread(os.path.join(args.images_folder, row.image_xpl)) / 255.
        all_mean_xpl[count, :] = np.mean(img_xpl, axis=(0, 1))

        img_ppl = cv2.imread(os.path.join(args.images_folder, row.image_ppl)) / 255.
        all_mean_ppl[count, :] = np.mean(img_ppl, axis=(0, 1))
        count += 1
        print("Entry {0} out of {1} done - {2:.2f} %".format(count, num_entries, (count/num_entries * 100)), end='\r')

    mean_xpl = np.mean(all_mean_xpl, axis=0)
    mean_ppl = np.mean(all_mean_ppl, axis=0)

    print("means done, xpl : {}, ppl : {}".format(mean_xpl, mean_ppl))


    print("computing std")
    std_xpl = np.sqrt(np.sum((all_mean_xpl - mean_xpl)**2, axis=0) / (num_entries - 1))
    std_ppl = np.sqrt(np.sum((all_mean_ppl - mean_ppl)**2, axis=0) / (num_entries - 1))

    print("std done, xpl : {}, ppl : {}".format(std_xpl, std_ppl))

    np.savez("mean_std.npz", mean_xpl=mean_xpl, mean_ppl=mean_ppl, std_xpl=std_xpl, std_ppl=std_ppl)
