from scrapy.settings import Settings
from scrapy  import signals
from scrapy.crawler import CrawlerProcess
from bgs_spider import BGSSpider
import argparse
import scrapy
import logging

logger = logging.getLogger("bgs."+__name__)

## Settings for broad crawling
settings = {
  'BOT_NAME' : 'bgs',
  'LOG_LEVEL' : 'INFO',
  'DEPTH_LIMIT' : 0,
  'DEPTH_PRIORITY' : 3,
  #'SCHEDULER_DISK_QUEUE': 'scrapy.squeues.PickleFifoDiskQueue',
  #'SCHEDULER_MEMORY_QUEUE': 'scrapy.squeues.FifoMemoryQueue',
  'MAX_MEM' : 4096, # in Mo
  'CONCURRENT_REQUESTS' : 100,
  'CONCURRENT_REQUESTS_PER_DOMAIN' : 16,
  'REACTOR_THREADPOOL_MAXSIZE' : 30,
  'COOKIES_ENABLED' : False,
  'DOWNLOAD_TIMEOUT' : 60,
  'ROBOTSTXT_OBEY' : False,
  'SPIDER_MIDDLEWARES' : {},
  'ITEM_PIPELINES' : {'build_dataset_pipeline.BuildDatasetPipeline' : 1},
  'USER_AGENT': 'Mozilla/54.0 (compatible; MSIE 7.0; Windows NT 5.1)',
  'AJAXCRAWL_ENABLED' : True
  }


results = None
def process_result(spider, reason):
        global results

        results = spider.get_result()
        logger.debug("result done")

def start_processing(spider_args_dict={}):
   """! @brief start_processing launch scrappy crawler inside the current process
        and build the networkx graph as each item is being processed by the spider.
        It return the built graph using queue

        @param queue the multiprocess queue
   """
   global settings, results


   logger.debug("Launching scrap items")
   process = CrawlerProcess(settings)
   process.crawl(BGSSpider, **spider_args_dict)

   for crawler in process.crawlers:
       crawler.signals.connect(process_result, signal=signals.spider_closed)

   process.start()

   results.to_csv("dataset.csv", index=False)

if __name__ == "__main__":

    parser = argparse.ArgumentParser()
    parser.add_argument("--images_folder", default="images", type=str, help="Path to folder where images are saved")
    args = parser.parse_args()

    start_processing({"start_url" : "https://www.bgs.ac.uk/data/britrocks/britrocks.cfc?method=listResults&pageSize=1000",
                    "images_path" : args.images_folder})
